module RmplusDevtools

  module ApplicationControllerPatch
    def self.included(base)
      base.extend(ClassMethods)
      base.send(:include, InstanceMethods)

      base.class_eval do
        after_action :rack_mini_profiler
      end
    end

    module ClassMethods
    end

    module InstanceMethods

      def rack_mini_profiler
        enable_profiling = (Setting.plugin_rmplus_devtools || {})['enable_profiling']
        if enable_profiling && User.current.active?
          profiling_user_ids = (Setting.plugin_rmplus_devtools || {})['user_to_profile_ids'] || []
          if profiling_user_ids.any? && User.where(id: profiling_user_ids).include?(User.current)
            Rack::MiniProfiler.authorize_request
          end
        end
      end

    end

  end
end