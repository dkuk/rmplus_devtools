require 'rmplus_devtools/rmplus_devtools'

Redmine::Plugin.register :rmplus_devtools do
  name 'RMPlus Devtools plugin'
  author 'Alexey Glukhov, Danil Kukhlevskiy'
  description 'Collection of tools useful for Redmine developers'
  version '1.1.0'
  url 'http://rmplus.pro/redmine/plugins/rmplus_devtools'
  author_url 'http://rmplus.pro'

  settings partial: 'settings/rmplus_devtools',
           default: { 'enable_profiling' => true,
                      'enable_oink' => false,
                      'user_to_profile_ids' => [1],
                      'enable_assets_listeners' => true }
end

Rails.application.config.after_initialize do
  RmplusDevtools::AssetsListener.check_listeners
end



Rails.application.config.to_prepare do
  ApplicationController.send(:include, RmplusDevtools::ApplicationControllerPatch) unless ApplicationController.included_modules.include?(RmplusDevtools::ApplicationControllerPatch)
end


########
# Direct Setting read is used for prevent of Setting object initialized before all plugins registred! Or later plugins will fail on tryin to read Setting.plugin_settings
########
rmplus_devtools_settings = ActiveRecord::Base.connection.select_all("SELECT value FROM settings WHERE name='plugin_rmplus_devtools'").first || {}
rmplus_devtools_settings = YAML.load(rmplus_devtools_settings['value'].to_s)

if rmplus_devtools_settings.is_a?(Hash) && rmplus_devtools_settings['enable_oink']
  Rails.application.middleware.use Oink::Middleware
  # Rails.application.middleware.use(Oink::Middleware, logger: Rails.logger)
end
